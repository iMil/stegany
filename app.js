const stegcloak = new StegCloak(true, false);
var app = new Vue({
  el: '#app',
  data: {
    secret: "",
    passwd: "",
    text: "",
    out: ""
  },
  methods: {
    hide: function () {
      if (this.secret.length == 0|| this.passwd.length == 0) {
        alert("Secret and password are needed")
        return
      }
      wl = this.text.split(" ")
      if (wl.length < 2) {
        alert("At least 2 words are needed for the cover text")
        return
      }
      this.out = stegcloak.hide(this.secret, this.passwd, this.text)
    },
    reveal: function () {
      if (this.passwd.length == 0 || this.text.length == 0) {
        alert("Password and text are needed")
        return
      }
      this.out = stegcloak.reveal(this.text, this.passwd)
    },
    init: function() {
      this.secret = this.passwd = this.text = this.out = ""
    }
  }
})

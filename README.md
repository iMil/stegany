### Stegany, a bootstrap frontend to [stegcloak][1]

`Stegany` is a self contained frontend to the awesome [stegcloak][1], a JavaScript module designed to hide secrets inside text by compressing and encrypting the secret before cloaking it with special unicode invisible characters.

[Stegcloak][1] has a fantastic [demo website][2] for which I unfortunately couldn't find any source code, so here I made a simple [Bootstrap][3] and [Vue.js][5] based user interface.

![Stegany user interface](https://imil.net/pics/imil.net/stegany.png)

#### Usage

Just like the project it was inspired from, `stegany` encrypts a _secret_ using a _password_ and hides it in a _cover text_.  The result is a magic text with the _secret_ hidden with invisible and encrypted characters.  
To _reveal_ the secret, enter the _password_ used to encrypt it, and paste the _magic text_ which the _secret_ will be _revealed_ from.

#### Legal

Do whatever you want with this code, as it is, except for the [stegcloak JavaScript][4], under the [WTFPL](http://www.wtfpl.net/).

In this repository, `stegcloak.min.js`, the main component taken from the [stegcloak][1] repository, is under the [MIT](https://github.com/KuroLabs/stegcloak/blob/master/LICENSE) license.

[1]: https://github.com/KuroLabs/stegcloak
[2]: https://stegcloak.surge.sh/
[3]: https://getbootstrap.com/
[4]: https://github.com/KuroLabs/stegcloak/blob/master/dist/stegcloak.min.js
[5]: https://vuejs.org/
